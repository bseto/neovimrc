#!/bin/bash

WORK_DIR=`pwd`


mkdir -p /home/byron/.local/share/nvim
sudo chown byron:byron /home/byron/.local/share/nvim -R

sudo apt-get install git -y
sudo apt-get install software-properties-common -y

sudo apt-get install python-dev python-pip python3-dev python3-pip -y

apt-get install curl -y

#bashrc
mv ~/.bashrc ~/.bashrc.bak
ln -s ${WORK_DIR}/.bashrc ~/.bashrc

#pluginstall
mkdir -p ~/.local/share/nvim/site/autoload
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

mkdir -p ~/.config/nvim
rm ~/.config/nvim/init.vim
ln -s ${WORK_DIR}/.nvimrc ~/.config/nvim/init.vim

sudo apt-get install exuberant-ctags -y

sudo apt-get install ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip -y
sudo apt-get install cmake -y


git clone https://github.com/neovim/neovim.git
cd neovim
make -j16
sudo make install

